import VueRouter from 'vue-router';

const routes = [ {
    path: '/',
    name: 'Login',
    component: () => import('../components/Login.vue')
},{
    path:'/index',
    name:'Index',
    component:()=>import('../components/Index.vue'),
    children:[{
        path: '/home',
        name: 'Home',
        // 增加meta 实现面包屑导航
        meta:{
          title: '首页'
        },
        component: () => import('../components/Home.vue')
    // },{
    //     path:'/Admin',
    //     name:'admin',
    //     meta:{
    //         title:'管理员管理'
    //     },
    //     component:()=>import('../components/admin/AdminManage.vue')
    // },
    //     {
    //         path:'/User',
    //         name:'user',
    //         meta:{
    //             title:'用户管理'
    //         },
    //         component:()=>import('../components/user/UserManage.vue')
        }]
}]

const router = new VueRouter({
    mode:'history',
    routes
})
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}

export function resetRouter() {
    router.matcher = new VueRouter({
        mode:'history',
        routes: []
    }).matcher
}
export default router;