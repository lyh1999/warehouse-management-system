##### springboot 初搭建

1 open文件夹

2 创建modules

3  创建测试类

```java
@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello World!  Tomcat";
    }
}
```

 注解@RestController: 这是一个Spring Boot的注解，表示这是一个RESTful Web服务控制器。它告诉Spring Boot，这个类将负责处理HTTP请求，并将响应作为方法返回值。

注解@GetMapping: 这是一个Spring MVC的注解，表示这个方法将处理HTTP GET请求。它告诉Spring MVC，当用户访问"/hello"路径时，将调用这个方法并返回"hello World! Tomcat"作为响应。

##### mybatis-plus使用

entity>User

```java
// 引入 lombok 的 @Data 注解，自动生成 getter、setter、toString 等方法
@Data
public class User {
    private int id;
    private String no;
    private String name;
    private String password;
    private int sex;
    private int roleId;
    private String phone;
    // 使用 @TableField 注解，将属性 isvalid 映射到数据库表的 isValid 列
    @TableField(value = "isValid")
    private String isvalid;
}
// 上述代码定义了一个 User 类，其中使用了 lombok 的 @Data 注解，自动生成了 getter、setter、toString 等方法。类中声明了几个私有属性，包括 id、no、name、password、sex、roleId、phone 和 isvalid。其中 isvalid 属性使用了 @TableField 注解，将其映射到数据库表的 isValid 列上。
```

mapper>UserMapper接

```java
// 使用 @Mapper 注解，将接口标识为 MyBatis 的 Mapper 接口
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
//上述代码定义了一个 UserMapper 接口，使用了 @Mapper 注解。这个接口继承了 BaseMapper<User> 接口，该接口是 MyBatis-Plus 框架提供的接口之一，用于对实体类 User 进行基本的 CRUD 操作。

//通过继承 BaseMapper<User> 接口，UserMapper 接口将自动继承一些常用的数据库操作方法，例如 selectById、insert、update、deleteById 等。可以直接在其他组件中注入 UserMapper 接口的实例，然后调用这些方法来操作数据库表对应的 User 实体类。
BaseMapper 后面的 <User> 表示泛型，用于指定当前 BaseMapper 接口所处理的实体类类型是 User。也就是说，BaseMapper<User> 接口提供了一组对 User 实体类进行基本 CRUD 操作的方法。

在 MyBatis-Plus 中，所有继承了 BaseMapper 接口的自定义 Mapper 接口都要指定实体类类型，以便框架能够正确地生成 SQL 语句并映射实体类属性和数据库表字段。

需要注意的是，BaseMapper 接口中定义的一些通用方法可能无法满足所有需求，此时可以在自定义 Mapper 接口中添加额外的方法来扩展功能。例如，可以在 UserMapper 接口中添加一个根据用户名查询用户的方法：

java
public interface UserMapper extends BaseMapper<User> {
    User selectByName(String name);
}
这样，就可以在其他组件中通过调用 selectByName 方法来查询用户数据了。
```

service>UserService

```java
public interface UserService extends IService<User> {
}
```

service>impl>UserServiceImpl

```java
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>implements UserService {
}
//上述代码定义了一个 UserService 接口，它继承了 IService<User> 接口。
// IService 是 MyBatis-Plus 框架提供的一个接口，用于定义 Service 层的基本操作方法。通过继承 IService<User> 接口，UserService 接口将继承一些常用的数据库操作方法，例如 save、update、removeById 等方法，用于对 User 实体进行增删改查操作。

通过使用这些基本的 CRUD 方法，可以在 Service 层中实现具体的业务逻辑，并调用对应的 Mapper 接口来操作数据库。接口中还可以自定义其他方法，用于满足特定的业务需求。
```

controller>HelloController

```java
// 使用 @RestController 注解，将类标识为控制器类，并自动将返回值转换为 JSON 格式
@RestController
public class HelloController {
    @GetMapping
    public String hello(){
        return "hello World!  Tomcat";
    }
	// 使用 @Autowired 注解，自动注入 UserService 的实例
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> list(){
        return userService.list();
    }
}
// 上述代码定义了一个 HelloController 类作为控制器类。使用 @RestController 注解标识该类是一个 REST 风格的控制器，可以处理 HTTP 请求并返回 JSON 格式的数据。

HelloController 类中定义了两个请求处理方法：

hello() 方法使用 @GetMapping 注解，表示处理 GET 请求。该方法返回一个字符串 "hello World! Tomcat"，当客户端访问根路径时会触发该方法。
list() 方法也使用 @GetMapping 注解，表示处理 GET 请求。该方法调用 userService.list() 方法来获取用户列表，并将结果以 JSON 格式返回给客户端。
在 HelloController 类中通过 @Autowired 注解将 userService 注入其中，以便在方法中调用 userService 的方法来处理业务逻辑。
```

application.yml

```yaml
server:
  port: 8090

spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/wms?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=GMT%2B8
    username: root
    password: lyh199910

```

这些包和文件之间的联系以及数据流转如下：

1. `entity` 包中的 `User` 类是一个普通的 JavaBean，用于封装用户信息。该类定义了一些私有属性以及对应的 getter 和 setter 方法。

2. `mapper` 包中的 `UserMapper` 接口继承了 MyBatis-Plus 框架提供的 `BaseMapper` 接口，该接口提供了一些基本的 CRUD 方法，可以通过 SQL 映射配置文件来将这些方法映射到实际的 SQL 语句上。

   在 `UserMapper` 接口中还可以定义自己的 SQL 方法，以满足特定业务需求。

3. `service` 包中的 `UserService` 接口继承了 MyBatis-Plus 框架提供的 `IService` 接口，该接口提供了一些常用的数据库操作方法，比如 `save`、`update`、`removeById` 等。`UserService` 接口还可以定义自己的方法，用于实现具体的业务逻辑。

   为了实现具体的业务逻辑，`UserService` 接口中需要调用 `UserMapper` 中的方法进行数据库操作，因此在 `UserService` 的实现类 `UserServiceImpl` 中需要注入 `UserMapper` 的实例，以便在方法中调用它的方法来实现数据库操作。

4. `controller` 包中的 `HelloController` 类作为控制器类，使用 `@RestController` 注解标识为 REST 风格的控制器。在该类中定义了两个请求处理方法，分别是 `hello()` 和 `list()` 方法。

   `hello()` 方法返回一个字符串，可以用于测试服务器是否正常启动。`list()` 方法调用 `UserService` 中的方法获取用户列表，并将结果以 JSON 格式返回给客户端。

数据流转路径如下：

1. 客户端发送 HTTP 请求到服务器，请求经过 SpringMVC 的分发器进行路由选择。路由选择成功后，将请求发送到对应的控制器类中。
2. `HelloController` 接收到请求后，根据请求路径选择相应的处理方法进行处理。
3. 在 `list()` 方法中，通过注入 `UserService` 的实例，调用 `UserService` 的方法获取用户列表，并将结果封装成 JSON 格式返回给客户端。
4. 在 `UserService` 的实现类中，通过注入 `UserMapper` 的实例，调用 `UserMapper` 的方法来实现具体的数据库操作。
5. 在 `UserMapper` 中，定义了一些 SQL 语句，它们在执行时需要依赖 MyBatis 运行时环境提供的组件（如 SqlSession 等）。
6. 在 `User` 实体类中，定义了一些属性和方法，用于封装用户信息。这些信息会在数据库操作时被读取或写入。
   通过以上步骤，客户端、控制器类、Service 层、Mapper 层以及数据库之间实现了数据的流转和交互。

###### 自定义mapper方法

```java
@RestController
public class HelloController {
    @GetMapping
    public String hello(){
        return "hello World!  Tomcat";
    }

    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> list(){

        return userService.listAll();

    }
}
```

```java
public interface UserService extends IService<User> {

//    给自定义方法写配置 xml
    List<User> listAll();

}
```

userServiceImpl类下  alt+enter 导入方法

```java
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>implements UserService {

    @Resource
//    注入userMapper
    private UserMapper userMapper;
    @Override
    public List<User> listAll() {
//        调用userMapper中的listAll方法
        return userMapper.listAll();
    }

}
```

```java
@Mapper
public interface UserMapper extends BaseMapper<User> {

    List<User> listAll();

}
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.wms.mapper.UserMapper">
    <select id="listAll" resultType="com.wms.entity.User">
        select * from user
    </select>
</mapper>
```

通过以上步骤，客户端、控制器类、Service 层、Mapper 层以及数据库之间实现了数据的流转和交互。

##### 自动生成代码

在pom.xml中 加入依赖

```xml
<dependency>
 <groupId>com.baomidou</groupId>
 <artifactId>mybatis-plus-generator</artifactId>
 <version>3.4.1</version>
</dependency>
<dependency>
 <groupId>org.freemarker</groupId>
 <artifactId>freemarker</artifactId>
 <version>2.3.30</version>
</dependency>
<dependency>
 <groupId>com.spring4all</groupId>
 <artifactId>spring-boot-starter-swagger</artifactId>
 <version>1.5.1.RELEASE</version>
</dependency>
```

添加生成器代码

mybatis-plus官网地址 https://baomidou.com/pages/d357af/

```java
package com.wms.common;

import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CodeGenerator {
    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder help = new StringBuilder();
        help.append("请输入" + tip + "：");
        System.out.println(help.toString());
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotBlank(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    /**
     * 操作步骤：
     *  1.修改数据源包括地址密码信息，对应代码标记：一、 下同
     *  2.模块配置，可以修改包名
     *  3.修改模板（这步可忽略）
     * @param args
     */
    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir")+"/wms";
        gc.setOutputDir(projectPath + "/src/main/java");
        gc.setAuthor("wms");
        gc.setOpen(false);
        gc.setSwagger2(true); //实体属性 Swagger2 注解
        gc.setBaseResultMap(true);// XML ResultMap
        gc.setBaseColumnList(true);// XML columList
        //去掉service接口首字母的I, 如DO为User则叫UserService
        gc.setServiceName("%sService");
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        // 一、修改数据源
        dsc.setUrl("jdbc:mysql://localhost:3306/wms02?useUnicode=true&characterEncoding=UTF8&useSSL=false");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("root");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        //pc.setModuleName(scanner("模块名"));
        // 二、模块配置
        pc.setParent("com.wms")
            .setEntity("entity")
            .setMapper("mapper")
            .setService("service")
            .setServiceImpl("service.impl")
            .setController("controller");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        /*
        cfg.setFileCreate(new IFileCreate() {
            @Override
            public boolean isCreate(ConfigBuilder configBuilder, FileType fileType, String filePath) {
                // 判断自定义文件夹是否需要创建
                checkDir("调用默认方法创建的目录，自定义目录用");
                if (fileType == FileType.MAPPER) {
                    // 已经生成 mapper 文件判断存在，不想重新生成返回 false
                    return !new File(filePath).exists();
                }
                // 允许生成模板文件
                return true;
            }
        });
        */
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        // 配置自定义输出模板
        //指定自定义模板路径，注意不要带上.ftl/.vm, 会根据使用的模板引擎自动识别
        // 三、修改模板
        /*templateConfig.setEntity("templates/entity2.java");
        templateConfig.setService("templates/service2.java");
        templateConfig.setController("templates/controller2.java");
        templateConfig.setMapper("templates/mapper2.java");
        templateConfig.setServiceImpl("templates/serviceimpl2.java");*/

        templateConfig.setXml(null);
        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        // strategy.setSuperEntityClass("你自己的父类实体,没有就不用设置!");
        //strategy.setSuperEntityClass("BaseEntity");
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        // 公共父类
        //strategy.setSuperControllerClass("BaseController");
        // strategy.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");
        // 写于父类中的公共字段
        // strategy.setSuperEntityColumns("id");
        strategy.setInclude(scanner("表名，多个英文逗号分割").split(","));
        strategy.setControllerMappingHyphenStyle(true);
        //strategy.setTablePrefix(pc.getModuleName() + "_");
        // 忽略表前缀tb_,比如说tb_user,直接映射成user对象
        // 四、注意是否要去掉表前缀
        //strategy.setTablePrefix("tb_");
        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }
}
```

右键运行codeGenerator  输入表名 回车 即可生成代码

书写测试类代码

```java
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @GetMapping("/list")
    public List<User> list(){
        return userService.list();
    }

}
```

访问路径 http://localhost:8090/user/list  两个GetMapping

##### 实现增删改查

```java
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> list() {
        return userService.list();
    }
    //新增
    @PostMapping("/save")
    public boolean save(@RequestBody User user){
        return userService.save(user);
    }
    //修改
    @PostMapping("/mod")
    public boolean mod(@RequestBody User user){
        return userService.updateById(user);
    }
    //新增或修改
    @PostMapping("/saveOrMod")
    public boolean saveOrMod(@RequestBody User user){
//        根据user是否有id 判断进行新增还是修改
        return userService.saveOrUpdate(user);
    }
    //删除

    @GetMapping("/delete")
    public boolean delete(Integer id){
        return userService.removeById(id);
    }
    //查询（模糊 匹配）
    @PostMapping("/listP")
    public List<User> listP(@RequestBody User user){
//        实现模糊查询
        LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName,user.getName());
        return userService.list(lambdaQueryWrapper);
//        匹配搜索 将like 改为 eq
    }
}
```

启动postman测试 

选择body  json格式

##### 实现数据分页

原生请求

```java
@PostMapping("/listPage")
public List<User> listPage(@RequestBody HashMap map){
    System.out.println(map); // 打印 {name=超级管理员, pageSize=10, pageNum=1}
    return null;
}
```

postman 传递{
    "name": "超级管理员",
	"pageSize":10,
	"pageNum":1
} 请求 

###### 入参封装

common 新建QueryPageMapper

```java
@Data
public class QueryPageParam {
//    默认值
    private static int PAGE_SIZE = 10;
    private static int PAGE_NUM = 1;
    private int  pageSize = PAGE_SIZE;
    private int  pageNum = PAGE_NUM;
}
```

```java
@PostMapping("/listPage")
public List<User> listPage(@RequestBody QueryPageParam param){
    System.out.println(param); //  打印 QueryPageParam(pageSize=10, pageNum=1)
    return null;
}
```

```java
public List<User> listPage(@RequestBody QueryPageParam param){
        System.out.println(param);
        System.out.println("num=" + param.getPageNum()+ "size="+ param.getPageSize()); // num=1 size=10
    }
```

```java
    @PostMapping("/listPage")
    public List<User> listPage(@RequestBody HashMap map){

        System.out.println("num="+(String)map.get("pageSize")); // num=10  pageSize类型必须为 String 

        return null;
    }
```

前端传递多个参数时

{
	"param":{
		"name": "超级管理员",
		"no":"db"
	},
	"pageSize":"10",
	"pageNum":"1"
}

```java
@Data
public class QueryPageParam {
//    默认值
    private static int PAGE_SIZE = 10;
    private static int PAGE_NUM = 1;
    private int  pageSize = PAGE_SIZE;
    private int  pageNum = PAGE_NUM;
    private HashMap param;
}
```

```java
 @PostMapping("/listPage")
    public List<User> listPage(@RequestBody QueryPageParam query){

        System.out.println("num=" + query.getPageNum()); // num=1
        System.out.println("size"+ query.getPageSize());//size=10
        System.out.println(query); // QueryPageParam(pageSize=10, pageNum=1, param={no=db, name=超级管理员, age=19})
        return null;
    }
```

```java
 @PostMapping("/listPage")
    public List<User> listPage(@RequestBody QueryPageParam query){

        System.out.println("num=" + query.getPageNum()); // num=1
        System.out.println("size="+ query.getPageSize());//size=10
        HashMap param = query.getParam();
        System.out.println("name="+  (String)param.get("name")); // name=超级管理员
        System.out.println(query);
        return null;
    }
```

###### 添加分页拦截器

添加mybatis-plus的分页拦截器  MybatisPlusInterceptor

https://baomidou.com/pages/2976a3/

```java
@Configuration
@MapperScan("scan.your.mapper.package")
public class MybatisPlusConfig {

    /**
     * 添加分页插件
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));//如果配置多个插件,切记分页最后添加
        //interceptor.addInnerInterceptor(new PaginationInnerInterceptor()); 如果有多数据源可以不配具体类型 否则都建议配上具体的DbType
        return interceptor;
    }
}
```

###### 编写分页mapper

```java
 Page<User> page = new Page<User>(query.getPageNum(),query.getPageSize()); // 几页 几条数据


 Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(1);
        page.setSize(10);
```

```java
@PostMapping("/listPage")
    public List<User> listPage(@RequestBody QueryPageParam query){

//        System.out.println("num=" + query.getPageNum()); // num=1
//        System.out.println("size="+ query.getPageSize());//size=10
        HashMap param = query.getParam();
        String name = (String)param.get("name");
//        System.out.println("name="+ name);
//        System.out.println(query);

        Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName, name);

        IPage result = userService.page(page, lambdaQueryWrapper);

        System.out.println("total = " + result.getTotal());

        return result.getRecords();

    }
```

###### ！！！Alt+enter生成自定义方法 statement (.xml)

###### 自定义SQL使用Wrapper

###### ！使用wrapper自定义SQL

userController

```java
 @PostMapping("/listPageC")
    public List<User> listPageC(@RequestBody QueryPageParam query){

        HashMap param = query.getParam();
        String name = (String)param.get("name");

        Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName, name);

        IPage result = userService.page(page);

        System.out.println("total = " + result.getTotal());

        return result.getRecords();

    }
```

```java
public interface UserService extends IService<User> {
    IPage pageC(IPage<User> page);
}
```

```java
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public IPage pageC(IPage<User> page) {
        return userMapper.pageC(page);
    }
}
```

```java
@Mapper
public interface UserMapper extends BaseMapper<User> {

    IPage pageC(IPage<User> page);

}
```

```xml
<select id="pageC" resultType="com.wms.entity.User">
    select * from user;
</select>
```

##### 返回给前端数据的封装

封装后返回的数据格式应为

{

Code:200/400,

Msg:'成功/失败',

Total:10,

Data:[] / {}

}

Result工具类

```java
@Data
public class Result {
    private int code;
    private String msg;
    private Long total;
    private Object data;

    public static Result fail(){
        return result(400, "失败", null, 0L);
    }

    public static Result success(){
        return result(200, "成功", null, 0L);
    }

    public static Result success(Object data){
        return result(200, "成功", data, 0L);
    }

    public static Result success(Object data,Long total){
        return result(200, "成功", data, total);
    }

    private static Result result(int code,String msg,Object data,Long total){
        Result res = new Result();
        res.setData(data);
        res.setMsg(msg);
        res.setCode(code);
        res.setTotal(total);
        return res;
    }
}
```

UserController测试类

```java
@PostMapping("/listPageC1")
public Result listPageC1(@RequestBody QueryPageParam query){

    HashMap param = query.getParam();
    String name = (String)param.get("name");

    Page<User> page = new Page<User>(); // 几页 几条数据
    page.setCurrent(query.getPageNum());
    page.setSize(query.getPageSize());

    LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    lambdaQueryWrapper.like(User::getName, name);

    IPage result = userService.page(page);

    System.out.println("total = " + result.getTotal());

    return Result.success(result.getRecords(),result.getTotal());

}
```

postman测试

```json
{
	"pageSize":10,
	"pageNum":1,
	"param":{
		"name":"",
		"no": "admin"
	}
}
```

返回结果

```json
{
    "code": 200,
    "msg": "成功",
    "total": 2,
    "data": [
        {
            "id": 1,
            "no": "admin",
            "name": "超级管理员",
            "password": "lyh199910",
            "age": 23,
            "sex": 1,
            "phone": "1223",
            "roleId": 1,
            "isvalid": "Y"
        },
        {
            "id": 2,
            "no": "admin",
            "name": "李华",
            "password": "123456",
            "age": 18,
            "sex": 1,
            "phone": "1223",
            "roleId": 1,
            "isvalid": "Y"
        }
    ]
}
```

##### 页面布局搭建

Container布局容器

Header 和 Aside组件搭建

菜单导航栏收缩 

header 点击图标 传递收缩值  传递父组件 父组件传递子组件 aside  控制collapse

跨组件通信

##### 安装axios与跨域处理

```javascript
  beforeMount() {
    this.loadGet();
    this.loadPost();
  },
  methods:{
    loadGet(){
      this.$axios.get('http://localhost:8090/user/list').then(res=>res.data).then(res=>{
        console.log(res)
      })
    },
    loadPost(){
      this.$axios.post('http://localhost:8090/user/listP',{name: "李"}).then(res=>res.data).then(res=>{
        console.log(res)
      })
    }
  }
```

对应userController里的requestMapping

```java
@PostMapping("/listP")
    public List<User> listP(@RequestBody User user){
//        实现模糊查询
        LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        if(StringUtils.isNotBlank(user.getName())){
            lambdaQueryWrapper.like(User::getName,user.getName());
        }
        lambdaQueryWrapper.like(User::getName,user.getName());
        return userService.list(lambdaQueryWrapper);
//        匹配搜索 将like 改为 eq
    }
```

springboot 配置跨域

```java
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                //是否发送Cookie
                .allowCredentials(true)
                //放行哪些原始域
                .allowedOriginPatterns("*")
                .allowedMethods(new String[]{"GET", "POST", "PUT", "DELETE"})
                .allowedHeaders("*")
                .exposedHeaders("*");
    }
}
```

设置全局地址  main.js

```js
Vue.prototype.$httpUrl = 'http://localhost:8090/'
```

```js
loadGet(){
    this.$axios.get(this.$httpUrl+'/user/list').then(res=>res.data).then(res=>{
        console.log(res)
    })
},
loadPost(){
    this.$axios.post(this.$httpUrl + '/user/listP',{name: "李"}).then(res=>res.data).then(res=>{
        console.log(res)
    })
}
```

###### request.js封装axios

##### 列表展示

获取列表数据  

###### v-slot或slot-scope用tag转换列数据   

###### header-cell-style设置表头样式

边框 

按钮   

后端返回结果封装

##### 分页查询

采用pagination 

```java
    @PostMapping("/listPageC1")
    public Result listPageC1(@RequestBody QueryPageParam query){

        HashMap param = query.getParam();
        String name = (String)param.get("name");

        Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(name) && !"null".equals(name)){
            lambdaQueryWrapper.like(User::getName, name);            
        }
        

        IPage result = userService.page(page);
        System.out.println("total = " + result.getTotal());
        return Result.success(result.getRecords(),result.getTotal());
    }
```

```js
 loadPost(){
      this.$axios.post(this.$httpUrl + '/user/listPageC1',{name: "",pageSize:this.pageSize,pageNum:this.pageNum})
          .then(res=>res.data).then(res=>{
        console.log(res)
        if(res.code == 200){
          this.tableData = res.data
        }else {
          this.$message('获取数据失败')
        }
      })
    }
```

##### 查询处理  传参查询

查询 清空数据  输入框 下拉框

回车事件查询 @keyup.enter.native

重置处理



实现自定义方法pageCC  userController类

```java
@PostMapping("/listPageC1")
public Result listPageC1(@RequestBody QueryPageParam query) {

    HashMap param = query.getParam();
    String name = (String)param.get("name");
    String sex = (String)param.get("sex");

    Page<User> page = new Page<User>(); // 几页 几条数据
    page.setCurrent(query.getPageNum());
    page.setSize(query.getPageSize());

    LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    if(StringUtils.isNotBlank(name) && !"null".equals(name)){
        lambdaQueryWrapper.like(User::getName,name);
    }

    if (StringUtils.isNotBlank(sex) && !"null".equals(sex)) {
        lambdaQueryWrapper.eq(User::getSex, sex);
    }

    IPage result = userService.pageCC(page,lambdaQueryWrapper);
    return Result.success(result.getRecords(), result.getTotal());
}
```

userService生成 

```java
public interface UserService extends IService<User> {

    IPage pageC(IPage<User> page);

    IPage pageCC(IPage<User> page, Wrapper wrapper);
}
```

userServiceImpl实现类实现

```java
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public IPage pageC(IPage<User> page) {
        return userMapper.pageC(page);
    }

    @Override
    public IPage pageCC(IPage<User> page, Wrapper wrapper) {
        return userMapper.pageCC(page, wrapper);
    }
}
```

userMapper实现

```java
@Mapper
public interface UserMapper extends BaseMapper<User> {
    IPage pageC(IPage<User> page);

    IPage pageCC(IPage<User> page, @Param(Constants.WRAPPER) Wrapper wrapper);

}
```

userMapper.xml实现

```xml
<select id="pageCC" resultType="com.wms.entity.User">
    select * from user ${ew.customSqlSegment}
</select>
```

这些代码之间的串联关系如下：

1. 在`userController`类中，定义了`listPageC1`方法，该方法接受一个`QueryPageParam`对象作为参数，并通过`@RequestBody`注解将请求体中的数据映射到该对象上。然后从`query`对象中获取参数 `name` 和 `sex`。
2. 创建了一个`Page<User>`对象，设置了当前页码和每页数据数量。
3. 创建了一个`LambdaQueryWrapper<User>`对象，用于构建查询条件。
4. 根据参数 `name` 进行模糊查询，通过`lambdaQueryWrapper.like(User::getName,name)`实现。
5. 根据参数 `sex` 进行精确查询，通过`lambdaQueryWrapper.eq(User::getSex, sex)`实现。
6. 调用`userService`的`pageCC`方法，传入 `page` 对象和 `lambdaQueryWrapper` 对象作为参数，执行数据库查询操作。
7. `userService`的实现类`UserServiceImpl`中注入了`UserMapper`对象，并实现了`pageCC`方法，调用`userMapper`的`pageCC`方法，并将结果返回。
8. `UserMapper`接口继承了`BaseMapper<User>`，定义了`pageCC`方法，该方法接受一个`IPage<User>`对象和一个`Wrapper`对象作为参数，并执行数据库查询操作。
9. `userMapper.xml`文件中定义了`pageCC`方法的具体SQL语句，使用了动态SQL的方式拼接查询语句。
10. 最终，查询结果被返回到`userController`中，并通过`Result`对象的方式进行封装返回给前端。

这些代码的作用分别是：

- `userController`：接收请求参数并调用`userService`进行业务逻辑处理，返回处理结果。
- `userService`：定义了对用户数据的业务操作方法。
- `UserServiceImpl`：实现了`UserService`接口的具体业务逻辑。
- `UserMapper`：定义了对用户数据的数据库操作方法。
- `userMapper.xml`：提供了具体的SQL语句，用于执行数据库查询操作。

通过以上代码的串联，完成了从控制器层到服务层再到数据访问层的数据流转和业务处理。

在上述代码中，可以将不同层的代码进行如下划分：

控制器层（Controller层）：

- `userController`类中的代码，包括`listPageC1`方法。

服务层（Service层）：

- `UserService`接口及其实现类`UserServiceImpl`中的代码。

数据访问层（DAO层）：

- `UserMapper`接口及其对应的`userMapper.xml`文件中的代码。

具体划分如下：

- 控制器层：`userController`类、`listPageC1`方法。
- 服务层：`UserService`接口、`UserServiceImpl`类。
- 数据访问层：`UserMapper`接口、`userMapper.xml`文件。

控制器层负责接收请求、处理请求参数、调用服务层进行业务处理，并将结果返回给前端。服务层负责实现具体的业务逻辑，调用数据访问层进行数据操作。数据访问层负责与数据库进行交互，执行具体的SQL语句进行数据查询、插入、更新等操作。

根据代码提供的信息，`Result`封装的对象属于控制器层（Controller层）。

在控制器层的代码中，我们可以看到`Result`对象被用来封装处理结果，并通过返回给前端。`Result`通常包含了请求的处理状态、响应消息以及需要返回给前端的数据。它的作用是将业务处理结果进行统一的格式化，方便前端进行解析和处理。

因此，`Result`封装的对象属于控制器层，用于封装并返回处理结果给前端。

##### 新增功能

新增按钮

###### 弹出窗口 (模态框)  

dialog + form

编写表单

```vue
<el-dialog
           title="提示"
           :visible.sync="centerDialogVisible"
           width="30%"
           center>
    <el-form ref="form" :model="form" label-width="80px">
        <el-form-item label="账号" prop="no">
            <el-col :span="20">
                <el-input v-model="form.no"></el-input>
            </el-col>
        </el-form-item>
        <el-form-item label="名字" prop="name">
            <el-col :span="20">
                <el-input v-model="form.name"></el-input>
            </el-col>
        </el-form-item>
        <el-form-item label="密码" prop="password">
            <el-col :span="20">
                <el-input v-model="form.password"></el-input>
            </el-col>
        </el-form-item>
        <el-form-item label="年龄" prop="age">
            <el-col :span="20">
                <el-input v-model="form.age"></el-input>
            </el-col>
        </el-form-item>
        <el-form-item label="性别">
            <el-radio-group v-model="form.sex">
                <el-radio label="1">男</el-radio>
                <el-radio label="0">女</el-radio>
            </el-radio-group>
        </el-form-item>
        <el-form-item label="电话" prop="phone">
            <el-col :span="20">
                <el-input v-model="form.phone"></el-input>
            </el-col>
        </el-form-item>
    </el-form>
    <span slot="footer" class="dialog-footer">
        <el-button @click="centerDialogVisible = false">取 消</el-button>
        <el-button type="primary" @click="save">确 定</el-button>
    </span>
</el-dialog>
```

###### 提交数据 提示信息 列表刷新

###### 数据校验

新增置空

```javascript
//清空表单
resetForm(){
    this.$refs.form.resetFields();
}
add(){
    this.centerDialogVisible = true
    this.$nextTick(()=>{
        this.resetForm()
    })
},
```

this.resetForm要与prop和model共同使用

使用方法 [vue中el-form resetFields()使用注意 （置空失效原因查找）-CSDN博客](https://blog.csdn.net/josiecici/article/details/118298293)

```javascript
save(){
    this.$refs.form.validate((valid) => {
        if (valid) {
            if(this.form.id){
                // this.doMod();
            }else{
                this.doSave();
            }
        } else {
            console.log('error submit!!');
            return false;
        }
    });
},
```

```java
 @GetMapping("/findByNo")
    public Result findByNo(@RequestParam String no){
        List list = userService.lambdaQuery().eq(User::getNo,no).list();
        return list.size()>0?Result.success(list):Result.fail();
    }
```

##### 修改功能

传递数据到表单form

提交数据到后台

表单重置

##### 删除功能

获取数据id

删除确认 

提交后台

表单重置

##### 登录功能

登录页面编写

后端查询

登录路由拦截

创建路由文件

```javascript
import VueRouter from 'vue-router';

const routes = [ {
    path: '/',
    name: 'Login',
    component: () => import('../components/Login.vue')
}]

const router = new VueRouter({
    mode:'history',
    routes
})

export default router;
```

main.js注册

##### 登录退出

展示名称

```javascript
// login时  sessionStorage.setItem("CurUser",JSON.stringify(res.data)) 
data() {
    return {
        //存储在sessionStorage中的用户信息为字符串类型 需要转换成对象
        user: JSON.parse(sessionStorage.getItem('CurUser'))
    }
}
```

退出登录事件

退出跳转 清空相关数据

退出确认

```javascript
logOut() {
    this.$confirm('您确定要退出登录吗?', '提示', {
        confirmButtonText: '确定',  //确认按钮的文字显示
        type: 'warning',
        center: true, //文字居中显示
    })
        .then(() => {
        this.$message({
            type: 'success',
            message: '退出登录成功'
        })

        this.$router.push("/")
        sessionStorage.clear()
    })
        .catch(() => {
        this.$message({
            type: 'info',
            message: '已取消退出登录'
        })
    })
},
```

##### 个人中心（Home）

页面编写

路由跳转

路由错误解决

```js
const VueRouterPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}
```

router-view是**渲染视图**的： 主要是构建 SPA (单页应用) 时， 方便渲染你指定路由对应的组件

`router-view`是一个内置的组件，用于显示路由对应的组件内容。与其他自定义组件不同，`router-view`无需显式地注册，因为它是Vue Router插件的一部分。

当你使用Vue Router插件时，在你的项目中安装并配置了Vue Router后，`router-view`组件会自动被注册。Vue Router会根据路由配置动态地将`router-view`组件插入到匹配的位置，并渲染对应的组件内容。

##### 菜单跳转

菜单增加router 高亮

配置子菜单

模拟动态menu

```java
data(){
    return {
        // isCollapse:false
        menu:[
            {
                menuClick:'Admin',
                menuName:'管路员管理',
                menuIcon:'el-icon-s-custom'
            },{
                menuClick:'User',
                menuName:'用户管理',
                menuIcon:'el-icon-user-solid'
            }
        ]
    }
}
```

```vue
<el-menu-item :index="'/'+item.menuclick" v-for="(item,i) in menu" :key="i">
    <i :class="item.menuicon"></i>
    <span slot="title">{{item.menuname}}</span>
</el-menu-item>
```

##### 动态路由 

设计menu表和数据

```sql
CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menuCode` varchar(8) DEFAULT NULL COMMENT '菜单编码',
  `menuName` varchar(16) DEFAULT NULL COMMENT '菜单名字',
  `menuLevel` varchar(2) DEFAULT NULL COMMENT '菜单级别',
  `menuParentCode` varchar(8) DEFAULT NULL COMMENT '菜单的父code',
  `menuClick` varchar(16) DEFAULT NULL COMMENT '点击触发的函数',
  `menuRight` varchar(8) DEFAULT NULL COMMENT '权限 0超级管理员，1表示管理员，2表示普通用户，可以用逗号组合使用',
  `menuComponent` varchar(200) DEFAULT NULL,
  `menuIcon` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


BEGIN;
INSERT INTO `menu` VALUES (1, '001', '管理员管理', '1', NULL, 'Admin', '0', 'admin/AdminManage.vue', 'el-icon-s-custom');
INSERT INTO `menu` VALUES (2, '002', '用户管理', '1', NULL, 'User', '0,1', 'user/UserManage.vue', 'el-icon-user-solid');
INSERT INTO `menu` VALUES (3, '003', '仓库管理', '1', NULL, 'Storage', '0,1', 'storage/StorageManage', 'el-icon-office-building');
INSERT INTO `menu` VALUES (4, '004', '物品分类管理', '1', NULL, 'Goodstype', '0,1', 'goodstype/GoodstypeManage', 'el-icon-menu');
INSERT INTO `menu` VALUES (5, '005', '物品管理 ', '1', NULL, 'Goods', '0,1,2', 'goods/GoodsManage', 'el-icon-s-management');
INSERT INTO `menu` VALUES (6, '006', '记录管理', '1', NULL, 'Record', '0,1,2', 'record/RecordManage', 'el-icon-s-order');
COMMIT;
```

生成menu对应的后端代码

运行CodeGenerator

生成器生成代码会在mapper文件处少@mapper注释 需手动添加

返回数据 

menuController.java

```java
@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;
    @GetMapping("/list")
    public Result list(@RequestParam String roleId){

        List list = menuService.lambdaQuery().like(Menu::getMenuright,roleId).list();
        return list.size()>0?Result.success(list):Result.fail();
    }
}

```

userController.java

```java
//    登录
@PostMapping("/login")
public Result login(@RequestBody User user) {
    List list  = userService.lambdaQuery().eq(User::getNo,user.getNo())
        .eq(User::getPassword,user.getPassword()).list();

    if(list.size()>0){
        User user1 = (User) list.get(0);
        List menulist = menuService.lambdaQuery().like(Menu::getMenuright,user1.getRoleId()).list();
        HashMap res = new HashMap();
        res.put("user",user1);
        res.put("menu",menulist);

        //            return Result.success(list.get(0));
        return Result.success(res);
    }

    return Result.fail();
}
```
login.vue

```javascript
sessionStorage.setItem("CurUser", JSON.stringify(res.data.user))
```

###### Vuex状态管理

yarn add vuex@3.0.0

编写store  main.js注册

生成menu数据

生成路由数据

路由列表  router.options.routes

##### 管理员管理 用户管理

管理员管理

```javascript
form:{
    id:'',
        no:'',
            name:'',
                password:'',
                    age:'',
                        phone:'',
                            sex:'0',
                                roleId:'1'
},
    loadPost(){
                this.$axios.post(this.$httpUrl+'/user/listPageC1',{
                    pageSize:this.pageSize,
                    pageNum:this.pageNum,
                    param:{
                        name:this.name,
                        sex:this.sex,
                        roleId:'1'
                    }
                }).then(res=>res.data).then(res=>{
                    console.log(res)
                    if(res.code==200){
                        this.tableData=res.data
                        this.total=res.total
                    }else{
                        alert('获取数据失败')
                    }

                })
            }
// 管理员显示和新增的数据权限都是 roleId: 1
```

用户管理

```javascript
form:{
    id:'',
        no:'',
            name:'',
                password:'',
                    age:'',
                        phone:'',
                            sex:'0',
                                roleId:'2'
},
    loadPost(){
        this.$axios.post(this.$httpUrl+'/user/listPageC1',{
            pageSize:this.pageSize,
            pageNum:this.pageNum,
            param:{
                name:this.name,
                sex:this.sex,
                roleId:'2'
            }
        }).then(res=>res.data).then(res=>{
            console.log(res)
            if(res.code==200){
                this.tableData=res.data
                this.total=res.total
            }else{
                alert('获取数据失败')
            }

        })
    }
```

##### 仓库管理

sql新建表

```mysql
DROP TABLE IF EXISTS `storage`;
CREATE TABLE `storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '仓库名',
  `remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```

CodeGenerator生成 storageMapper

post接口 postman测试传递参数 要按对应格式传递 不然会报400错误

```mysql
http://localhost:8090/storage/listPage
{
    "pageSize": 10,
    "pageNum": 1,
    "param": {
        "name": ""
    }
}
```

##### 物品类型管理

mysql表设计

根据表生成后端代码

编写后端增删改查

1 复制 contoller

2 没有pageCC方法   复制修改 service 和impl实现类

3  修改mapper.java  和mapper.xml

postman测试查询

前端代码

##### 物品管理

mysql表设计

根据表生成后端代码

编写后端增删改查

1 复制 contoller

2 没有pageCC方法   复制修改 service 和impl实现类

3  修改mapper.java  和mapper.xml

postman测试查询

前端代码

仓库和分类列表展示

页面加载前 先加载storage的 list

```java
 @GetMapping("/list")
    public Result list(){

        List list = storageService.list();
        return list.size()>0?Result.success(list):Result.fail();
    }
```

组件使用

```vue
<el-select v-model="value" placeholder="请选择">
    <el-option
      v-for="item in options"
      :key="item.value"
      :label="item.label"
      :value="item.value">
    </el-option>
  </el-select>

data() {
    return {
    options: [{
            value: '选项1',
            label: '黄金糕'
        }, {
            value: '选项2',
            label: '双皮奶'
        }, {
            value: '选项3',
            label: '蚵仔煎'
        }, {
            value: '选项4',
            label: '龙须面'
        }, {
            value: '选项5',
            label: '北京烤鸭'
        }],
        	value: ''
    }
}
```

官方组件采用  value 和label 这样的数组 我们也需要将数组整理为类似数组

但避免麻烦 也可以改成我们自己的数组格式

```vue
<el-select v-model="storage" placeholder="请选择仓库" style="margin-left: 5px;">
    <el-option
               v-for="item in storageData"
               :key="item.id"
               :label="item.name"
               :value="item.id">
    </el-option>
</el-select>
```

```java
   @PostMapping("/listPage")
    public Result listPage(@RequestBody QueryPageParam query) {

        HashMap param = query.getParam();
//        接收name goodstype storage 传值并按值查询返回
        String name = (String)param.get("name");
        String goodstype = (String)param.get("goodstype");
        String storage = (String)param.get("storage");

        Page<Goods> page = new Page<Goods>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<Goods> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(name) && !"null".equals(name)){
            lambdaQueryWrapper.like(Goods::getName,name);
            System.out.println("name = " + name);
        }
        if(StringUtils.isNotBlank(goodstype) && !"null".equals(goodstype)){
            lambdaQueryWrapper.eq(Goods::getGoodstype,goodstype);
        }
        if(StringUtils.isNotBlank(storage) && !"null".equals(storage)){
            lambdaQueryWrapper.eq(Goods::getStorage,storage);
        }

        IPage result = goodsService.pageCC(page,lambdaQueryWrapper);
        return Result.success(result.getRecords(), result.getTotal());
    }
```

##### 记录管理

sql表设计

自动生成后端代码

编写后端查询代码

前端代码编写

###### 优化

连表查询

列表展示商品名 仓库 分类名

按物品名查询， 仓库， 分类查询

##### 出入库管理

element 单选对象支持  highlight-current-row属性实现

选择操作人 内层嵌套模态框

##### 权限控制优化

出入库权限控制

```vue
<el-button type="primary" style="margin-left: 5px;" @click="add" v-if="user.roleId!=2">新增</el-button>
<el-button type="primary" style="margin-left: 5px;" @click="inGoods" v-if="user.roleId!=2">
    入库
</el-button>
<el-button type="primary" style="margin-left: 5px;" @click="outGoods" v-if="user.roleId!=2">
    出库
</el-button>
```

用户加权限值 menuRight  后端控制   前端v-if实现按钮控制

记录查询权限控制

##### 前后端项目部署

pom.xml  右键 add as maven project

##### Vuex持久化

浏览器刷新 丢失state中数据  

vuex-persistedstate

yarn add vuex-persistedstate

在 store>index.js引入

```typescript
import createPersistedState from 'vuex-persistedstate'
plugins: [createPersistedState()]
```

解决路由丢失

app.vue

```java
data() {
    return {
        user:JSON.parse(sessionStorage.getItem('CurUser'))
    }
},
watch:{
    '$store.state.menu':{
        handler(newVal,oldVal){
            console.log(newVal)
                if(!oldVal && this.user && this.user.no){
                    this.$store.commit('setMenu',newVal))
                }
        },
        deep:true,
        immediate:true
    }
}
```



v-if实现按钮控制

记录查询权限控制
