package com.wms.common;

import lombok.Data;

/**
 * @author Admin
 * @title: Result
 * @projectName springboot-vue-warehouse-management-system
 * @description: Result
 * @date 2023/11/6 21:33
 */

@Data
public class Result {
    private int code;
    private String msg;
    private Long total;
    private Object data;

    public static Result fail(){
        return result(400, "失败", null, 0L);
    }

    public static Result success(){
        return result(200, "成功", null, 0L);
    }

    public static Result success(Object data){
        return result(200, "成功", data, 0L);
    }

    public static Result success(Object data,Long total){
        return result(200, "成功", data, total);
    }

    private static Result result(int code,String msg,Object data,Long total){
        Result res = new Result();
        res.setData(data);
        res.setMsg(msg);
        res.setCode(code);
        res.setTotal(total);
        return res;
    }
}
