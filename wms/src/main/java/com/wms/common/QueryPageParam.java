package com.wms.common;

import lombok.Data;

import java.util.HashMap;

/**
 * @author Admin
 * @title: QueryPageParam
 * @projectName springboot-vue-warehouse-management-system
 * @description: QueryPageParam
 * @date 2023/11/4 16:52
 */

@Data
public class QueryPageParam {
//    默认值
    private static int PAGE_SIZE = 10;
    private static int PAGE_NUM = 1;
    private int  pageSize = PAGE_SIZE;
    private int  pageNum = PAGE_NUM;
    private HashMap param = new HashMap();
}
