package com.wms.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wms.common.QueryPageParam;
import com.wms.common.Result;
import com.wms.entity.Menu;
import com.wms.entity.Storage;
import com.wms.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author wms
 * @since 2023-11-25
 */
@RestController
@RequestMapping("/storage")
public class StorageController {
    @Autowired
    private StorageService storageService;
    // 新增
    @PostMapping("/save")
    public Result save(@RequestBody Storage storage) {
        return storageService.save(storage)?Result.success():Result.fail();
    }

    // 修改
    @PostMapping("/mod")
    public boolean mod(@RequestBody Storage storage) {
        return storageService.updateById(storage);
    }

    @PostMapping("/update")
    public Result update(@RequestBody Storage storage) {
        return storageService.updateById(storage)?Result.success():Result.fail();
    }

    // 新增或修改
    @PostMapping("/saveOrMod")
    public boolean saveOrMod(@RequestBody Storage storage) {
        // 根据Storage是否有id 判断进行新增还是修改
        return storageService.saveOrUpdate(storage);
    }
    // 删除

    @GetMapping("/delete")
    public Result delete(Integer id) {
        return storageService.removeById(id)?Result.success():Result.fail();
    }

    @PostMapping("/listPage")
    public Result listPage(@RequestBody QueryPageParam query) {

        HashMap param = query.getParam();
        String name = (String)param.get("name");


        Page<Storage> page = new Page<Storage>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<Storage> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(name) && !"null".equals(name)){
            lambdaQueryWrapper.like(Storage::getName,name);
            System.out.println("name = " + name);
        }

        IPage result = storageService.pageCC(page,lambdaQueryWrapper);
        return Result.success(result.getRecords(), result.getTotal());
    }

    @GetMapping("/list")
    public Result list(){

        List list = storageService.list();
        return list.size()>0?Result.success(list):Result.fail();
    }
}
