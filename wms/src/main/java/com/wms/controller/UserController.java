package com.wms.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wms.common.QueryPageParam;
import com.wms.common.Result;
import com.wms.entity.Menu;
import com.wms.entity.User;
import com.wms.service.MenuService;
import com.wms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.naming.Name;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author wms
 * @since 2023-11-04
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private MenuService menuService;

    @GetMapping("/list")
    public List<User> list() {
        return userService.list();
    }

    // 新增
    @PostMapping("/save")
    public Result save(@RequestBody User user) {
        return userService.save(user)?Result.success():Result.fail();
    }

    // 修改
    @PostMapping("/mod")
    public boolean mod(@RequestBody User user) {
        return userService.updateById(user);
    }

    @PostMapping("/update")
    public Result update(@RequestBody User user) {
        return userService.updateById(user)?Result.success():Result.fail();
    }

    // 新增或修改
    @PostMapping("/saveOrMod")
    public boolean saveOrMod(@RequestBody User user) {
        // 根据user是否有id 判断进行新增还是修改
        return userService.saveOrUpdate(user);
    }
    // 删除

    @GetMapping("/delete")
    public Result delete(Integer id) {
        return userService.removeById(id)?Result.success():Result.fail();
    }

    // 查询（模糊 匹配）
    // @PostMapping("/listP")
    // public List<User> listP(@RequestBody User user){
    //// 实现模糊查询
    // LambdaQueryWrapper <User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
    //
    // if(StringUtils.isNotBlank(user.getName())){
    // lambdaQueryWrapper.like(User::getName,user.getName());
    // }
    // lambdaQueryWrapper.like(User::getName,user.getName());
    // return userService.list(lambdaQueryWrapper);
    //// 匹配搜索 将like 改为 eq
    // }
    // Result封装
    @PostMapping("/listP")
    public Result listP(@RequestBody User user) {
        // 实现模糊查询
        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();

        if (StringUtils.isNotBlank(user.getName())) {
            lambdaQueryWrapper.like(User::getName, user.getName());
        }
        lambdaQueryWrapper.like(User::getName, user.getName());

        return Result.success(userService.list(lambdaQueryWrapper));
        // 匹配搜索 将like 改为 eq
    }

    @PostMapping("/listPage")
    // public List<User> listPage(@RequestBody HashMap map){
    public List<User> listPage(@RequestBody QueryPageParam query) {
//        param与传递的参数param要保持一致
        HashMap param = query.getParam();
        String name = (String) param.get("name");
        System.out.println("name==" + (String) param.get("name"));
        /* System.out.println("no==="+(String)param.get("no")); */
        /*
         * LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper();
         * lambdaQueryWrapper.eq(User::getName,user.getName());
         *
         * return userService.list(lambdaQueryWrapper);
         */

        Page<User> page = new Page();
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.like(User::getName, name);

        IPage result = userService.page(page, lambdaQueryWrapper);

        System.out.println("total==" + result.getTotal());

        return result.getRecords();
    }

    @PostMapping("/listPageC")
    public List<User> listPageC(@RequestBody QueryPageParam query) {

        HashMap param = query.getParam();
        String name = (String) param.get("name");
        String roleId =  (String) param.get("roleId");

        Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.like(User::getName, name);
//        pageC的userMapper查询方法 查询全部数据
        IPage result = userService.pageC(page);

        System.out.println("total = " + result.getTotal());

        return result.getRecords();

    }

    @PostMapping("/listPageC1")
    public Result listPageC1(@RequestBody QueryPageParam query) {

        HashMap param = query.getParam();
        String name = (String)param.get("name");
        String sex = (String)param.get("sex");
        String roleId =  (String)param.get("roleId");

        Page<User> page = new Page<User>(); // 几页 几条数据
        page.setCurrent(query.getPageNum());
        page.setSize(query.getPageSize());

        LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        if(StringUtils.isNotBlank(name) && !"null".equals(name)){
            lambdaQueryWrapper.like(User::getName,name);
            System.out.println("name = " + name);
        }

        if (StringUtils.isNotBlank(sex) && !"null".equals(sex)) {
            lambdaQueryWrapper.eq(User::getSex, sex);
            System.out.println("sex = " + sex);
        }

        if (StringUtils.isNotBlank(roleId) && !"null".equals(sex)) {
            lambdaQueryWrapper.eq(User::getRoleId, sex);
            System.out.println("roleId = " + roleId);
        }

        System.out.println("name = " + name);
        IPage result = userService.pageCC(page,lambdaQueryWrapper);
        System.out.println("total = " + result.getTotal());
        System.out.println(result.getRecords());
        return Result.success(result.getRecords(), result.getTotal());
    }

    @GetMapping("/findByNo")
    public Result findByNo(@RequestParam String no){
        List list = userService.lambdaQuery().eq(User::getNo,no).list();
        return list.size()>0?Result.success(list):Result.fail();
    }
//    登录
    @PostMapping("/login")
    public Result login(@RequestBody User user) {
        List list  = userService.lambdaQuery().eq(User::getNo,user.getNo())
                .eq(User::getPassword,user.getPassword()).list();

        if(list.size()>0){
            User user1 = (User) list.get(0);
            List menulist = menuService.lambdaQuery().like(Menu::getMenuright,user1.getRoleId()).list();
            HashMap res = new HashMap();
            res.put("user",user1);
            res.put("menu",menulist);

//            return Result.success(list.get(0));
            return Result.success(res);
        }

        return Result.fail();
    }


}
