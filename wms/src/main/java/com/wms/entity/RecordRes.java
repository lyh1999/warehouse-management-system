package com.wms.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author Admin
 * @title: RecordRes
 * @projectName springboot-vue-warehouse-management-system
 * @description: RecordRes
 * @date 2023/11/26 12:05
 */
@Data
public class RecordRes {
    private Integer id;
    private String username;
    private String adminname;
    private String goodsname;
    private String storagename;
    private String goodstypename;
    private Integer count;
    private LocalDateTime createtime;
    private String remark;
}
